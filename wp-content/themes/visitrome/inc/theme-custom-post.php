<?php
function cptui_register_my_cpts() {
	/**
	 * Post Type: Locations.
	 */

	$labels = [
		"name" => __( "Locations", "visitrome" ),
		"singular_name" => __( "Location", "visitrome" ),
		"menu_name" => __( "Locations", "visitrome" ),
		"all_items" => __( "All Locations", "visitrome" ),
		"add_new" => __( "Add new", "visitrome" ),
		"add_new_item" => __( "Add new Location", "visitrome" ),
		"edit_item" => __( "Edit Location", "visitrome" ),
		"new_item" => __( "New Location", "visitrome" ),
		"view_item" => __( "View Location", "visitrome" ),
		"view_items" => __( "View Locations", "visitrome" ),
		"search_items" => __( "Search Locations", "visitrome" ),
		"not_found" => __( "No Locations found", "visitrome" ),
		"not_found_in_trash" => __( "No Locations found in trash", "visitrome" ),
		"parent" => __( "Parent Location:", "visitrome" ),
		"featured_image" => __( "Featured image for this Location", "visitrome" ),
		"set_featured_image" => __( "Set featured image for this Location", "visitrome" ),
		"remove_featured_image" => __( "Remove featured image for this Location", "visitrome" ),
		"use_featured_image" => __( "Use as featured image for this Location", "visitrome" ),
		"archives" => __( "Location archives", "visitrome" ),
		"insert_into_item" => __( "Insert into Location", "visitrome" ),
		"uploaded_to_this_item" => __( "Upload to this Location", "visitrome" ),
		"filter_items_list" => __( "Filter Locations list", "visitrome" ),
		"items_list_navigation" => __( "Locations list navigation", "visitrome" ),
		"items_list" => __( "Locations list", "visitrome" ),
		"attributes" => __( "Locations attributes", "visitrome" ),
		"name_admin_bar" => __( "Location", "visitrome" ),
		"item_published" => __( "Location published", "visitrome" ),
		"item_published_privately" => __( "Location published privately.", "visitrome" ),
		"item_reverted_to_draft" => __( "Location reverted to draft.", "visitrome" ),
		"item_scheduled" => __( "Location scheduled", "visitrome" ),
		"item_updated" => __( "Location updated.", "visitrome" ),
		"parent_item_colon" => __( "Parent Location:", "visitrome" ),
	];

	$args = [
		"label" => __( "Locations", "visitrome" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "vr_locations", "with_front" => true ],
		"query_var" => true,
		"menu_icon" => "dashicons-location-alt",
		"supports" => [ "title" ],
	];

	register_post_type( "vr_locations", $args );

	/**
	 * Post Type: Hotels.
	 */

	$labels = [
		"name" => __( "Hotels", "visitrome" ),
		"singular_name" => __( "Hotel", "visitrome" ),
		"menu_name" => __( "Hotels", "visitrome" ),
		"all_items" => __( "All Hotels", "visitrome" ),
		"add_new" => __( "Add new", "visitrome" ),
		"add_new_item" => __( "Add new Hotel", "visitrome" ),
		"edit_item" => __( "Edit Hotel", "visitrome" ),
		"new_item" => __( "New Hotel", "visitrome" ),
		"view_item" => __( "View Hotel", "visitrome" ),
		"view_items" => __( "View Hotels", "visitrome" ),
		"search_items" => __( "Search Hotels", "visitrome" ),
		"not_found" => __( "No Hotels found", "visitrome" ),
		"not_found_in_trash" => __( "No Hotels found in trash", "visitrome" ),
		"parent" => __( "Parent Hotel:", "visitrome" ),
		"featured_image" => __( "Featured image for this Hotel", "visitrome" ),
		"set_featured_image" => __( "Set featured image for this Hotel", "visitrome" ),
		"remove_featured_image" => __( "Remove featured image for this Hotel", "visitrome" ),
		"use_featured_image" => __( "Use as featured image for this Hotel", "visitrome" ),
		"archives" => __( "Hotel archives", "visitrome" ),
		"insert_into_item" => __( "Insert into Hotel", "visitrome" ),
		"uploaded_to_this_item" => __( "Upload to this Hotel", "visitrome" ),
		"filter_items_list" => __( "Filter Hotels list", "visitrome" ),
		"items_list_navigation" => __( "Hotels list navigation", "visitrome" ),
		"items_list" => __( "Hotels list", "visitrome" ),
		"attributes" => __( "Hotels attributes", "visitrome" ),
		"name_admin_bar" => __( "Hotel", "visitrome" ),
		"item_published" => __( "Hotel published", "visitrome" ),
		"item_published_privately" => __( "Hotel published privately.", "visitrome" ),
		"item_reverted_to_draft" => __( "Hotel reverted to draft.", "visitrome" ),
		"item_scheduled" => __( "Hotel scheduled", "visitrome" ),
		"item_updated" => __( "Hotel updated.", "visitrome" ),
		"parent_item_colon" => __( "Parent Hotel:", "visitrome" ),
	];

	$args = [
		"label" => __( "Hotels", "visitrome" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "vr_hotels", "with_front" => true ],
		"query_var" => true,
		"menu_icon" => "dashicons-building",
		"supports" => [ "title", "editor", "thumbnail" ],
		"taxonomies" => [ "hotel_facilities", "hotel_popular_facilities" ],
	];

	register_post_type( "vr_hotels", $args );

	/**
	 * Post Type: Tours .
	 */

	$labels = [
		"name" => __( "Tours ", "visitrome" ),
		"singular_name" => __( "Tour", "visitrome" ),
		"menu_name" => __( "Tours", "visitrome" ),
		"all_items" => __( "All Tours", "visitrome" ),
		"add_new" => __( "Add new", "visitrome" ),
		"add_new_item" => __( "Add new Tour", "visitrome" ),
		"edit_item" => __( "Edit Tour", "visitrome" ),
		"new_item" => __( "New Tour", "visitrome" ),
		"view_item" => __( "View Tour", "visitrome" ),
		"view_items" => __( "View Tours", "visitrome" ),
		"search_items" => __( "Search Tours", "visitrome" ),
		"not_found" => __( "No Tours  found", "visitrome" ),
		"not_found_in_trash" => __( "No Tours  found in trash", "visitrome" ),
		"parent" => __( "Parent Tour:", "visitrome" ),
		"featured_image" => __( "Featured image for this Tour", "visitrome" ),
		"set_featured_image" => __( "Set featured image for this Tour", "visitrome" ),
		"remove_featured_image" => __( "Remove featured image for this Tour", "visitrome" ),
		"use_featured_image" => __( "Use as featured image for this Tour", "visitrome" ),
		"archives" => __( "Tour archives", "visitrome" ),
		"insert_into_item" => __( "Insert into Tour", "visitrome" ),
		"uploaded_to_this_item" => __( "Upload to this Tour", "visitrome" ),
		"filter_items_list" => __( "Filter Tours  list", "visitrome" ),
		"items_list_navigation" => __( "Tours  list navigation", "visitrome" ),
		"items_list" => __( "Tours  list", "visitrome" ),
		"attributes" => __( "Tours  attributes", "visitrome" ),
		"name_admin_bar" => __( "Tour", "visitrome" ),
		"item_published" => __( "Tour published", "visitrome" ),
		"item_published_privately" => __( "Tour published privately.", "visitrome" ),
		"item_reverted_to_draft" => __( "Tour reverted to draft.", "visitrome" ),
		"item_scheduled" => __( "Tour scheduled", "visitrome" ),
		"item_updated" => __( "Tour updated.", "visitrome" ),
		"parent_item_colon" => __( "Parent Tour:", "visitrome" ),
	];

	$args = [
		"label" => __( "Tours ", "visitrome" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "vr_tours", "with_front" => true ],
		"query_var" => true,
		"menu_icon" => "dashicons-palmtree",
		"supports" => [ "title", "editor", "thumbnail" ],
	];

	register_post_type( "vr_tours", $args );
}
add_action( 'init', 'cptui_register_my_cpts' );
function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Hotel Facilities.
	 */

	$labels = [
		"name" => __( "Hotel Facilities", "visitrome" ),
		"singular_name" => __( "Hotel Facilitiy", "visitrome" ),
		"menu_name" => __( "Hotel Facilities", "visitrome" ),
		"all_items" => __( "All Hotel Facilities", "visitrome" ),
		"edit_item" => __( "Edit Hotel Facilitiy", "visitrome" ),
		"view_item" => __( "View Hotel Facilitiy", "visitrome" ),
		"update_item" => __( "Update Hotel Facilitiy name", "visitrome" ),
		"add_new_item" => __( "Add new Hotel Facilitiy", "visitrome" ),
		"new_item_name" => __( "New Hotel Facilitiy name", "visitrome" ),
		"parent_item" => __( "Parent Hotel Facilitiy", "visitrome" ),
		"parent_item_colon" => __( "Parent Hotel Facilitiy:", "visitrome" ),
		"search_items" => __( "Search Hotel Facilities", "visitrome" ),
		"popular_items" => __( "Popular Hotel Facilities", "visitrome" ),
		"separate_items_with_commas" => __( "Separate Hotel Facilities with commas", "visitrome" ),
		"add_or_remove_items" => __( "Add or remove Hotel Facilities", "visitrome" ),
		"choose_from_most_used" => __( "Choose from the most used Hotel Facilities", "visitrome" ),
		"not_found" => __( "No Hotel Facilities found", "visitrome" ),
		"no_terms" => __( "No Hotel Facilities", "visitrome" ),
		"items_list_navigation" => __( "Hotel Facilities list navigation", "visitrome" ),
		"items_list" => __( "Hotel Facilities list", "visitrome" ),
	];

	$args = [
		"label" => __( "Hotel Facilities", "visitrome" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'hotel_facilities', 'with_front' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "hotel_facilities",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		];
	register_taxonomy( "hotel_facilities", [ "vr_hotels" ], $args );

	/**
	 * Taxonomy: Hotel Popular Facilities.
	 */

	$labels = [
		"name" => __( "Hotel Popular Facilities", "visitrome" ),
		"singular_name" => __( "Hotel Popular Facility", "visitrome" ),
		"menu_name" => __( "Hotel Popular Facilities", "visitrome" ),
		"all_items" => __( "All Hotel Popular Facilities", "visitrome" ),
		"edit_item" => __( "Edit Hotel Popular Facility", "visitrome" ),
		"view_item" => __( "View Hotel Popular Facility", "visitrome" ),
		"update_item" => __( "Update Hotel Popular Facility name", "visitrome" ),
		"add_new_item" => __( "Add new Hotel Popular Facility", "visitrome" ),
		"new_item_name" => __( "New Hotel Popular Facility name", "visitrome" ),
		"parent_item" => __( "Parent Hotel Popular Facility", "visitrome" ),
		"parent_item_colon" => __( "Parent Hotel Popular Facility:", "visitrome" ),
		"search_items" => __( "Search Hotel Popular Facilities", "visitrome" ),
		"popular_items" => __( "Popular Hotel Popular Facilities", "visitrome" ),
		"separate_items_with_commas" => __( "Separate Hotel Popular Facilities with commas", "visitrome" ),
		"add_or_remove_items" => __( "Add or remove Hotel Popular Facilities", "visitrome" ),
		"choose_from_most_used" => __( "Choose from the most used Hotel Popular Facilities", "visitrome" ),
		"not_found" => __( "No Hotel Popular Facilities found", "visitrome" ),
		"no_terms" => __( "No Hotel Popular Facilities", "visitrome" ),
		"items_list_navigation" => __( "Hotel Popular Facilities list navigation", "visitrome" ),
		"items_list" => __( "Hotel Popular Facilities list", "visitrome" ),
	];

	$args = [
		"label" => __( "Hotel Popular Facilities", "visitrome" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'hotel_popular_facilities', 'with_front' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "hotel_popular_facilities",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		];
	register_taxonomy( "hotel_popular_facilities", [ "vr_hotels" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes' );
?>