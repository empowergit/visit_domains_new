<?php
// Add Theme Options in option tree
function custom_theme_options() {
  /* OptionTree is not loaded yet, or this is not an admin request */
  if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
    return false;

  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( 'option_tree_settings', array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array(
    'contextual_help' => array(
      'content'       => array( 
        array(
          'id'        => 'general_help',
          'title'     => 'General',
          'content'   => '<p>Help content goes here!</p>'
        )
      ),
      'sidebar'       => '<p>Sidebar content goes here!</p>',
    ),
    'sections'        => array(
      array(
        'id'          => 'general',
        'title'       => 'General'
      ),
      array(
        'id'          => 'visit_domain_social_link_section',
        'title'       => 'Social Links'
      ),
      array(
        'id'          => 'visit_domain_card_accepted_section',
        'title'       => 'Accepted cards'
      ),
      array(
        'id'          => 'visit_domain_currency_section',
        'title'       => 'Currencies'
      ),
      array(
        'id'          => 'visit_domain_api_configure',
        'title'       => 'API Configure'
      )
    ),
    'settings'        => array(
       array(
         'id'          => 'visit_domain_google_api_key',
         'label'       => 'Google API key',
         'desc'        => '',
         'std'         => '',
         'type'        => 'text',
         'class'        => '',
         'operator'        => 'and',
         'section'        => 'visit_domain_api_configure'
       ),
       array(
         'id'          => 'visit_domain_google_api_marker',
         'label'       => 'Google API Map Marker',
         'desc'        => '',
         'std'         => '',
         'type'        => 'upload',
         'class'        => '',
         'operator'        => 'and',
         'section'        => 'visit_domain_api_configure'
       ),
       array(
         'id'          => 'visit_domain_contact_no',
         'label'       => 'Contact no',
         'desc'        => '',
         'std'         => '',
         'type'        => 'text',
         'rows'        => '',
         'post_type'   => '',
         'taxonomy'    => '',
         'min_max_step'        => '',
         'class'        => '',
         'condition'        => '',
         'operator'        => 'and',
         'section'        => 'general'
       ),    	
       array(
         'id'          => 'visit_domain_copyright',
         'label'       => 'Copyright',
         'desc'        => '',
         'std'         => '',
         'type'        => 'text',
         'rows'        => '',
         'post_type'   => '',
         'taxonomy'    => '',
         'min_max_step'        => '',
         'class'        => '',
         'condition'        => '',
         'operator'        => 'and',
         'section'        => 'general'
       ),
       array(
         'id'          => 'visit_domain_social_links',
         'label'       => 'Social Links',
         'desc'        => '',
         'std'         => '',
         'type'        => 'social-links',
         'rows'        => '',
         'post_type'   => '',
         'taxonomy'    => '',
         'min_max_step'        => '',
         'class'        => '',
         'condition'        => '',
         'operator'        => 'and',
         'section'        => 'visit_domain_social_link_section'
       ),
              array(
        'id'          => 'visit_domain_accepted_card_list',
        'label'       => 'Accepted cards',
        'desc'        => 'Add, remove accepted payment cards',
        'std'         => array(
                            array(
                                  'title' => 'Master Card',
                                  'visit_domain_payment_card_img' => get_template_directory_uri() . '/img/card/mastercard.png'
                            ),
                             array(
                                  'title' => 'JCB',
                                  'visit_domain_payment_card_img' => get_template_directory_uri() . '/img/card/jcb.png'
                            ),
                             array(
                                  'title' => 'Union Pay',
                                  'visit_domain_payment_card_img' => get_template_directory_uri() . '/img/card/unionpay.png'
                            ),
                             array(
                                  'title' => 'VISA',
                                  'visit_domain_payment_card_img' => get_template_directory_uri() . '/img/card/visa.png'
                            ),
                             array(
                                  'title' => 'American Express',
                                  'visit_domain_payment_card_img' => get_template_directory_uri() . '/img/card/americanexpress.png'
                            ) 
                         ),
        'type'        => 'list-item',
        'section'     => 'visit_domain_card_accepted_section',
        'class'       => '',
        'choices'     => array(),
        'settings'    => array(
          array(
            'id'      => 'visit_domain_payment_card_img',
            'label'   => 'Image',
            'desc'    => 'Image',
            'type'    => 'upload'
          )
        )
      ),
       array(
        'id'          => 'visit_domain_currency_list',
        'label'       => 'List of currencies',
        'desc'        => 'Add, remove a kind of currency for payment',
        'std'         => array(
                            array(
                                  'title' => 'USD',
                                  'visit_domain_currency_name' => 'USD',
                                  'visit_domain_currency_symbol' => '$',
                                  'visit_domain_currency_rate' => 1,
                                  'visit_domain_currency_thousand_separator' => '.',
                                  'visit_domain_currency_decimal_separator' => ','
                            )
                         ),
        'type'        => 'list-item',
        'section'     => 'visit_domain_currency_section',
        'class'       => '',
        'choices'     => array(),
        'settings'    => array(
          array(
            'id'      => 'visit_domain_currency_name',
            'label'   => 'Currency Name',
            'desc'    => '',
            'std'     => '',
            'type'    => 'select',
            'class'   => 'unique_currency',
            'operator' => 'and',
            'choices' => getAllCurrency()
          ),
          array(
            'id'      => 'visit_domain_currency_symbol',
            'label'   => 'Currency Symbol',
            'desc'    => '',
            'std'     => '',
            'type'    => 'text',
            'class'   => '',
            'operator' => 'and',
            'choices' => array()
          ),
          array(
            'id'      => 'visit_domain_currency_rate',
            'label'   => 'Exchange rate',
            'desc'    => 'Exchange rate vs Primary Currency',
            'std'     => '',
            'type'    => 'text',
            'class'   => '',
            'operator' => 'and',
            'choices' => array()
          ),
          array(
            'id'      => 'visit_domain_currency_thousand_separator',
            'label'   => 'Thousand Separator',
            'desc'    => 'Optional. Specifies what string to use for thousands separator.',
            'std'     => '',
            'type'    => 'text',
            'class'   => '',
            'operator' => 'and',
            'choices' => array()
          ),
          array(
            'id'      => 'visit_domain_currency_decimal_separator',
            'label'   => 'Decimal Separator',
            'desc'    => 'Optional. Specifies what string to use for decimal point.',
            'std'     => '',
            'type'    => 'text',
            'class'   => '',
            'operator' => 'and',
            'choices' => array()
          )
        )
      ),
      array(
        'id'      => 'visit_domain_primary_currency',
        'label'   => 'Primary Currency',
        'desc'    => 'Select a unit of currency as main currency',
        'std'     => 'USD',
        'type'    => 'select',
        'class'   => '',
        'operator' => 'and',
        'choices' => getCurrencyList(),
        'section' => 'visit_domain_currency_section'    
      ), 
    )
  );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( 'option_tree_settings', $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;
  
}
add_action( 'init', 'custom_theme_options', 1 );

// Get all currency list
function getAllCurrency(){
	            $all_currency = [
                'ALL' => 'Albania Lek',
                'DZD ' => 'Algeria',
                'AFN' => 'Afghanistan Afghani',
                'ARS' => 'Argentina Peso',
                'AWG' => 'Aruba Guilder',
                'AUD' => 'Australia Dollar',
                'AZN' => 'Azerbaijan New Manat',
                'BSD' => 'Bahamas Dollar',
                'BHD' => 'Bahraini Dinar',
                'BBD' => 'Barbados Dollar',
                'BDT' => 'Bangladeshi taka',
                'BYN' => 'Belarus Ruble',
                'BZD' => 'Belize Dollar',
                'BMD' => 'Bermuda Dollar',
                'BOB' => 'Bolivia Boliviano',
                'BAM' => 'Bosnia and Herzegovina Convertible Marka',
                'BWP' => 'Botswana Pula',
                'BGN' => 'Bulgaria Lev',
                'BRL' => 'Brazil Real',
                'BND' => 'Brunei Darussalam Dollar',
                'KHR' => 'Cambodia Riel',
                'CAD' => 'Canada Dollar',
                'KYD' => 'Cayman Islands Dollar',
                'CLP' => 'Chile Peso',
                'CNY' => 'China Yuan Renminbi',
                'COP' => 'Colombia Peso',
                'CRC' => 'Costa Rica Colon',
                'HRK' => 'Croatia Kuna',
                'CUP' => 'Cuba Peso',
                'CZK' => 'Czech Republic Koruna',
                'DKK' => 'Denmark Krone',
                'DOP' => 'Dominican Republic Peso',
                'XCD' => 'East Caribbean Dollar',
                'EGP' => 'Egypt Pound',
                'SVC' => 'El Salvador Colon',
                'EEK' => 'Estonia Kroon',
                'EUR' => 'Euro Member Countries',
                'FKP' => 'Falkland Islands (Malvinas) Pound',
                'FJD' => 'Fiji Dollar',
                'GHC' => 'Ghana Cedis',
                'GIP' => 'Gibraltar Pound',
                'GTQ' => 'Guatemala Quetzal',
                'GGP' => 'Guernsey Pound',
                'GYD' => 'Guyana Dollar',
                'GEL' => 'Georgia',
                'HNL' => 'Honduras Lempira',
                'HKD' => 'Hong Kong Dollar',
                'HUF' => 'Hungary Forint',
                'ISK' => 'Iceland Krona',
                'INR' => 'India Rupee',
                'IDR' => 'Indonesia Rupiah',
                'IRR' => 'Iran Rial',
                'IMP' => 'Isle of Man Pound',
                'ILS' => 'Israel Shekel',
                'JMD' => 'Jamaica Dollar',
                'JPY' => 'Japan Yen',
                'JEP' => 'Jersey Pound',
                'KZT' => 'Kazakhstan Tenge',
                'KPW' => 'Korea (North) Won',
                'KRW' => 'Korea (South) Won',
                'KGS' => 'Kyrgyzstan Som',
                'KDS' => 'Kenyan Shilling',
                'KSH' => 'Kenyan shilling',
                'LAK' => 'Laos Kip',
                'LVL' => 'Latvia Lat',
                'LBP' => 'Lebanon Pound',
                'LRD' => 'Liberia Dollar',
                'LTL' => 'Lithuania Litas',
                'MKD' => 'Macedonia Denar',
                'MYR' => 'Malaysia Ringgit',
                'MUR' => 'Mauritius Rupee',
                'MXN' => 'Mexico Peso',
                'MNT' => 'Mongolia Tughrik',
                'MMK' => 'Myanmar Kyats',
                'MAD' => 'Morocco Dirhams',
                'MZN' => 'Mozambique Metical',
                'MGA' => 'Malagasy ariary',
                'NAD' => 'Namibia Dollar',
                'NPR' => 'Nepal Rupee',
                'ANG' => 'Netherlands Antilles Guilder',
                'NZD' => 'New Zealand Dollar',
                'NIO' => 'Nicaragua Cordoba',
                'NGN' => 'Nigeria Naira',
                'NOK' => 'Norway Krone',
                'OMR' => 'Oman Rial',
                'PKR' => 'Pakistan Rupee',
                'PAB' => 'Panama Balboa',
                'PYG' => 'Paraguay Guarani',
                'PEN' => 'Peru Nuevo Sol',
                'SOL' => 'Peruvian Sol',
                'PHP' => 'Philippines Peso',
                'PLN' => 'Poland Zloty',
                'QAR' => 'Qatar Riyal',
                'RON' => 'Romania New Leu',
                'RUB' => 'Russia Ruble',
                'RWF' => 'Rwandan Frank',
                'SHP' => 'Saint Helena Pound',
                'SAR' => 'Saudi Arabia Riyal',
                'RSD' => 'Serbia Dinar',
                'SCR' => 'Seychelles Rupee',
                'SGD' => 'Singapore Dollar',
                'SBD' => 'Solomon Islands Dollar',
                'SOS' => 'Somalia Shilling',
                'ZAR' => 'South Africa Rand',
                'LKR' => 'Sri Lanka Rupee',
                'SEK' => 'Sweden Krona',
                'CHF' => 'Switzerland Franc',
                'SRD' => 'Suriname Dollar',
                'SYP' => 'Syria Pound',
                'TWD' => 'Taiwan New Dollar',
                'THB' => 'Thailand Baht',
                'TTD' => 'Trinidad and Tobago Dollar',
                'TRY' => 'Turkey Lira',
                'TRL' => 'Turkey Lira',
                'TVD' => 'Tuvalu Dollar',
                'TD' => 'Tunisian Dinar',
                'TZS' => 'Tanzanian Shilling',
                'UAH' => 'Ukraine Hryvna',
                'AED' => 'United Arab Emirates',
                'GBP' => 'United Kingdom Pound',
                'USD' => 'United States Dollar',
                'UYU' => 'Uruguay Peso',
                'UZS' => 'Uzbekistan Som',
                'UGX' => 'Ugandian Shilling',
                'VEF' => 'Venezuela Bolivar',
                'VND' => 'Viet Nam Dong',
                'YER' => 'Yemen Rial',
                'CFA' => 'West African Franc',
                'ZWD' => 'Zimbabwe Dollar',
                'ZMW' => 'Zambian Kwacha'
            ];
            $a = [];
            foreach ($all_currency as $key => $value) {
                $a[] = [
                    'value' => $key,
                    'label' => $value . '(' . $key . ' )'
                ];
            }
             return $a;
}

// Get Saved Currency list
function getCurrencyList(){
  $rows =array();
  $settings = get_option("option_tree");
  if(!empty($settings["visit_domain_currency_list"])){
    foreach($settings["visit_domain_currency_list"] as $value){
      $rows[]=$value;
    }
  }
  $choice = [];
  if(!empty($rows)){
  foreach ($rows as $key => $value) {
        $choice[] = [

            'label' => $value['title'],
            'value' => $value['visit_domain_currency_name']
        ];
    }
  }else{
        $choice[] = [

            'label' => "USD",
            'value' => "USD"
        ];    
  }    
  return $choice;
}
?>