<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package VisitRome
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'visitrome' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$visitrome_description = get_bloginfo( 'description', 'display' );
			if ( $visitrome_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $visitrome_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'visitrome' ); ?></button>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			) );
			?>
		</nav><!-- #site-navigation -->
		<?php
  if ( function_exists( 'ot_get_option' ) ) {
    $social_media_channels = array(
      "Facebook" => "fa fa-facebook-square",
      "Twitter" => "fa fa-twitter-square",
      "Google+" => "fa fa-google-plus-square",
      "LinkedIn" => "fa fa-linkedin-square",
      "Pinterest" => "fa fa-pinterest-square",
      "Youtube" => "fa fa-youtube-square"
    );
    
    $visit_domain_social_links = ot_get_option( 'visit_domain_social_links' );

  };
?>

<ul class="social-media-icons">
  <?php 
    if ( $visit_domain_social_links ) {
      foreach ( $visit_domain_social_links as $key=>$icon ) {
        if(!empty($icon['href'])){  
  ?>
  <li>
  	<a href="<?php echo $icon['href'];?>" title="<?php echo $icon['title'];?>">
  	     <i class="<?php echo $social_media_channels[$icon['name']];?>"></i>
  	</a>
  </li>
<?php    
       }
     }  
    }
  ?>
</ul>


<?php
  $list = ot_get_option("visit_domain_currency_list");
?>
<ul class="dropdown-menu">
        <?php
            if ( !empty( $list ) ) {
                foreach ( $list as $key => $value ) {
                    
                        echo '<li><a href="' . esc_url( add_query_arg( 'currency', $value[ 'visit_domain_currency_name' ] ) ) . '">' . esc_html($value[ 'visit_domain_currency_name' ]) . '</a>
                          </li>';
                }
            }
        ?>
    </ul>


	</header><!-- #masthead -->

	<div id="content" class="site-content">
