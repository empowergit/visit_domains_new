// Scripts for Hotel Section
jQuery( ".hotel_address .acf-input-wrap input" ).on("keyup", function(){	
  var address = jQuery(this).val();
  jQuery(".location_on_map .search").val(address);
  jQuery( ".location_on_map .search" ).focus();
  jQuery( ".hotel_address .acf-input-wrap input" ).focus();
});

jQuery(".unique_currency").change(function(){
  var currency = jQuery(this).val();
  var id = jQuery(this).attr('id');
  jQuery('#'+id).after("");
  jQuery.ajax({
	    type: "POST",
	    data: {'action': 'checkCurrencyExists', 'currency': currency},
	    url: ajaxUrl,
	    success: function (result) {
	       if(result=='0'){
             jQuery('#setting_'+id).after("<div class='errorCurrency'>Already Exists.Please choose different.</div>");
	       }else{
	       	 jQuery('#'+id).after("");
	       }
	    }
  }); 
});


// Scripts for Tour Section

jQuery( ".tour_address .acf-input-wrap input" ).on("keyup", function(){  
  var address = jQuery(this).val();
  jQuery(".location_on_map .search").val(address);
  jQuery( ".location_on_map .search" ).focus();
  jQuery( ".tour_address .acf-input-wrap input" ).focus();
});

jQuery( document ).ready(function() {
    loadPriceSetting();
});

function loadPriceSetting(id=""){
  if(id == ""){
     var id = jQuery(".tour_price_type .acf-input select").val();
  }
  if(id == "price_by_person"){
     jQuery(".tour_adult_price").show();
     jQuery(".tour_child_price").show();
     jQuery(".tour_infant_price").show();
     jQuery(".tour_base_price").hide();

     jQuery(".tour_adult_price_avail").show();
     jQuery(".tour_child_price_avail").show();
     jQuery(".tour_infant_price_avail").show();
     jQuery(".tour_base_price_avail").hide(); 
  }else{
     jQuery(".tour_adult_price").hide();
     jQuery(".tour_child_price").hide();
     jQuery(".tour_infant_price").hide();
     jQuery(".tour_base_price").show();

     jQuery(".tour_adult_price_avail").hide();
     jQuery(".tour_child_price_avail").hide();
     jQuery(".tour_infant_price_avail").hide();
     jQuery(".tour_base_price_avail").show();
  }
}

jQuery(".tour_price_type .acf-input select").on("change", function(){
  var id = jQuery(this).val();
  loadPriceSetting(id);
  jQuery(".acf-google-map").click();
});
