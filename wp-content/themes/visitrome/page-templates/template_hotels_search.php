<?php
   /* Template Name: Hotel Search Result Page */
   
   get_header();
   ?>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
   <?php
    while ( have_posts() ) :
      the_post();
      get_template_part( 'template-parts/content', 'page' );

    endwhile; // End of the loop.
    ?>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
<style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
    
<?php
  $key = ot_get_option("visit_domain_google_api_key");
  if(empty($key)){
    $key = "AIzaSyBNBkTy4fR0wc7UyYJrOPasx_5J8EPZ5M4";
  }


?>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=<?php echo $key;?>">
</script>

<div id="primary" class="content-area">
   <main id="main" class="site-main">
      <div class="row">
         <div class="col-md-4">
            <div data-role="rangeslider">
               Min:
               <div class="min_price_res"></div>
               <input type="range" name="min_price" id="min_price" value="0" min="0" max="1000" onchange="search_hotels();">
               Max:
               <div class="max_price_res"></div>
               <input type="range" name="max_price" id="max_price" value="800" min="0" max="1000" onchange="search_hotels();">
            </div>
            <div class="sidebar">
               <?php
                  $terms = get_terms( array('taxonomy' => 'hotel_popular_facilities','hide_empty' => false) );
                  echo count($terms);
                  ?>
               <ul>
                  <?php
                     foreach($terms as $val){
                     ?>
                  <li><input type="checkbox" name="hotel_popular_facilities" value="<?php echo $val->term_id;?>" onclick="search_hotels();"/> <?php echo $val->name;?></li>
                  <?php   }
                     ?>
               </ul>
               <ul>
                  <?php for($i=1;$i<=12;$i++){?>
                  <li><input type="checkbox" name="hotel_rating_standard" value="<?php echo $i;?>.0" onclick="search_hotels();">Star-<?php echo $i;?></li>
                  <?php }?>
               </ul>
            </div>
         </div>
         <div class="col-md-8">
            <div class="ajax_content"></div>
            <a href="javascript:void(0);" class="btn btn-primary"  data-toggle="modal" data-target="#mapModal">Map View</a>
            
         </div>
      </div>
   </main>
   <!-- #main -->
</div>
<!-- #primary -->
<?php
   get_footer();
   ?>
<script type="text/javascript">
   jQuery( document ).ready(function() {
       search_hotels();
   });  
   function search_hotels(){
         jQuery(".ajax_content").html("<img src='<?php echo get_template_directory_uri();?>/img/loader.gif'/>");
         var hotel_popular_facilities = [];
         jQuery('input[name="hotel_popular_facilities"]:checked').each(function() {
            hotel_popular_facilities.push(this.value);
         })
   
         var hotel_rating_standard = [];
         jQuery('input[name="hotel_rating_standard"]:checked').each(function() {
            hotel_rating_standard.push(this.value);
         })
   
         var min_price = jQuery("#min_price").val();
         var max_price = jQuery("#max_price").val();
   
         jQuery(".min_price_res").html(min_price);
         jQuery(".max_price_res").html(max_price);
   
   
           jQuery.ajax({
                type: "POST",
                data: {'action': 'search_hotels', 'hotel_popular_facilities': hotel_popular_facilities,'hotel_rating_standard':hotel_rating_standard,'min_price':min_price,'max_price':max_price},
                url: "<?php echo admin_url("admin-ajax.php");?>",
                success: function (result) {
                   var obj= JSON.parse(result);
                   if(obj.status_code == "1"){
                        jQuery(".ajax_content").html(obj.result);                    
                        if(obj.map_json == null){
                           jQuery(".map_content").hide();
                        }else{
                          jQuery(".map_content").show();
                          initialize(obj.map_json);
                        }
                   }else{
                       jQuery(".map_content").hide();
                       jQuery(".ajax_content").html("<div class='alert alert-danger'>"+obj.message+"</div>");   
                   }
                }
           });
   }
  function initialize(json) {
      var locations = json;
      //console.log(locations);
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: new google.maps.LatLng(41.89066819, 12.48443476),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var infowindow = new google.maps.InfoWindow();

      var marker, i;

      var map_icon = '<?php echo ot_get_option("visit_domain_google_api_marker");?>';

      for (i = 0; i < locations.length; i++) {  
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          map: map,
          icon: map_icon
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));
      }
  }
</script>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  

</head>
<body>


 

  <!-- Modal -->
  <div class="modal fade" id="mapModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Map View</h4>
        </div>
        <div class="modal-body">
          <div class="map_content" style="display: none;">
              <div id="map"></div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  


</body>
</html>
