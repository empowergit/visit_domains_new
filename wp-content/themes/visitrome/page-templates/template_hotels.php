<?php
/* Template Name: Hotel Listing Page */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php

         $the_posts = get_posts(array('post_type' => 'vr_hotels'));
         $i=1;
         foreach($the_posts as $val){
         	$post_id = $val->ID;
         	$location_on_map=get_post_meta($post_id, 'location_on_map', true);
            $latitude=  $location_on_map["lat"];
            $longitude= $location_on_map["lng"];

            

            $hotel_address =$val->post_title;
            $hotel_address .= "<a href=".get_permalink($post_id)." target=_blank>".get_post_meta($post_id, 'hotel_address', true)."</a>";
            $rows[]=array($hotel_address,$latitude,$longitude,$i);
            $i++;

         }


		?>
<style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
    <div id="map"></div>
		<script type="text/javascript">
			var json ='<?php echo json_encode($rows);?>';
			var object =JSON.parse(json);
			//console.log(object);
  function initialize() {
    var locations = object;

    console.log(locations);

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(41.89066819, 12.48443476),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' +
      'callback=initialize&key=AIzaSyBNBkTy4fR0wc7UyYJrOPasx_5J8EPZ5M4';
  document.body.appendChild(script);
}

window.onload = loadScript;
  </script>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
