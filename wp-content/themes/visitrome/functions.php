<?php
// Include the Theme Custom posts
//require_once dirname( __FILE__ ) . '/inc/theme-custom-post.php';

// Include Advanced Custom fields
//require_once dirname( __FILE__ ) . '/inc/theme-custom-fields.php';

// Include Theme Setting Options
require_once dirname( __FILE__ ) . '/inc/theme-custom-setting-options.php';


/**
 * VisitRome functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package VisitRome
 */

if ( ! function_exists( 'visitrome_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function visitrome_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on VisitRome, use a find and replace
		 * to change 'visitrome' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'visitrome', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'visitrome' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'visitrome_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'visitrome_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function visitrome_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'visitrome_content_width', 640 );
}
add_action( 'after_setup_theme', 'visitrome_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function visitrome_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'visitrome' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'visitrome' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'visitrome_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function visitrome_scripts() {
	wp_enqueue_style( 'visitrome-style', get_stylesheet_uri() );

	wp_enqueue_script( 'visitrome-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'visitrome-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'visitrome_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
// Admin CSS
add_action('admin_head', 'visitrome_admin_panel_css');
function visitrome_admin_panel_css() {
  $url = get_stylesheet_directory_uri() . '/admin/admin.css';	
  echo '<link href="'.$url.'" rel="stylesheet"/>';
}

// Admin JS
function visitrome_admin_js() {
	//global $post_type;;
    echo '<script>var ajaxUrl="'.admin_url("admin-ajax.php").'"</script>';
	//if( 'vr_hotels' == $post_type )
    $url = get_stylesheet_directory_uri() . '/admin/admin.js';
    echo '"<script type="text/javascript" src="'. $url . '"></script>"';
}
add_action('admin_footer', 'visitrome_admin_js');


// Google Map API key for Advanced Custom fields
function my_acf_google_map_api( $api ){
  $key = ot_get_option("visit_domain_google_api_key");
  if(!empty($key)){
    $api['key'] = $key;
  }else{
    $api['key'] ="AIzaSyBNBkTy4fR0wc7UyYJrOPasx_5J8EPZ5M4";
  }
	return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');


// Convert Currency
function format_money($price){
	    $money = (float)$price;
        $base = ot_get_option('visit_domain_primary_currency');
        if(!empty($_SESSION["current_currency"])){
	        if($_SESSION["current_currency"] == $base){
	          $base = ot_get_option('visit_domain_primary_currency');  	
	        }else{
	           $base = $_SESSION["current_currency"];	
	        }
        }
	    $list = ot_get_option("visit_domain_currency_list");
	    foreach($list as $value){
	      if($value["visit_domain_currency_name"]==$base){
	      	$symbol = $value["visit_domain_currency_symbol"];
	      	$precision="2";
	      	$rate = $value["visit_domain_currency_rate"];
	      	$thousand_separator = $value["visit_domain_currency_thousand_separator"];
	      	$decimal_separator = $value["visit_domain_currency_decimal_separator"];
	      	if(!empty($_SESSION["current_currency"])){
		      	 if($_SESSION["current_currency"] != ot_get_option('visit_domain_primary_currency')){

	                $money=$money*$rate; 	
	             }
            } 
	      	$money = round($money, 2);
	      	$money = number_format((float)$money, (int)$precision, $decimal_separator, $thousand_separator);
	      	$money_string = $symbol . " " . $money;
	      }	
	    }
        return $money_string;
}

// Check Currency exists using AJAX
add_action("wp_ajax_checkCurrencyExists", "check_currency_exists");
add_action("wp_ajax_nopriv_checkCurrencyExists", "check_currency_exists");
function check_currency_exists(){
	$currency = $_POST["currency"];
	$settings = get_option("option_tree");
	if(!empty($settings)){
        foreach($settings["visit_domain_currency_list"] as $value){
        	$rows[]=$value["visit_domain_currency_name"];
        }

        if(in_array($currency, $rows)){
         	echo "0";
			wp_die();
		}else{
			echo "1";
		    wp_die();
		}
	}else{
            echo "1";
		    wp_die();
	}
}

// On theme activation show required plugins message
require_once dirname( __FILE__ ) . '/lib/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'visitrome_require_plugins' ); 
function visitrome_require_plugins() {
    $plugins = array(
        array(
            'name'               => 'OptionTree',
            'slug'               => 'OptionTree',
            'source'             => 'st-option-tree.zip', 
            'required'           => true, // this plugin is required
            'version'            => '2.7.3', // the user must use version 2.7.3 (or higher) of this plugin
            'force_activation'   => false, // this plugin is going to stay activated unless the user switches to another theme
        ),
        array(
            'name'               => 'Advanced Custom Fields PRO',
            'slug'               => 'Advanced-Custom-Fields-PRO',
            'source'             => 'advanced-custom-fields-pro-master.zip', 
            'required'           => true, // this plugin is required
            'version'            => '5.7.10', // the user must use version 5.7.10 (or higher) of this plugin
            'force_activation'   => false, // this plugin is going to stay activated unless the user switches to another theme
        ),
        array(
            'name'               => 'Easy Google Fonts',
            'slug'               => 'easy-google-fonts',
            'source'             => 'easy-google-fonts.zip', 
            'required'           => true, // this plugin is required
            'version'            => '1.4.4', // the user must use version 5.7.10 (or higher) of this plugin
            'force_activation'   => false, // this plugin is going to stay activated unless the user switches to another theme
        )
        
    );

    $config = array(
        'id'           => 'visitrome-tgmpa', 
        'default_path' => get_stylesheet_directory() . '/lib/plugins/', 
        'menu'         => 'visitrome-install-required-plugins', 
        'has_notices'  => true, 
        'dismissable'  => false, 
        'dismiss_msg'  => 'The following plugins are required for proper functioning of theme', 
        'is_automatic' => true, 
        'message'      => '<!--Hey there.-->', 
        'strings'      => array() 
    );

    tgmpa( $plugins, $config );
}

add_action('init','visitrome_init_function');
function visitrome_init_function(){
   if(!empty($_GET["currency"])){
      $_SESSION["current_currency"]=$_GET["currency"];
   }
}

add_action('acf/validate_save_post', 'visitrome_acf_validate_save_post', 10, 0);
function visitrome_acf_validate_save_post(){
    $start = $_POST['acf']['field_5e535b42d051d'];
    $start = new DateTime($start);

    $end = $_POST['acf']['field_5e535b6dd051e'];
    $end = new DateTime($end);

// check custom $_POST data
    if ($start > $end) {
        acf_add_validation_error("acf[field_5e535b6dd051e]", 'Check Out Date should be greater than or Equal to Check In');
    }
}


add_action("wp_ajax_search_hotels", "search_hotels");
add_action("wp_ajax_nopriv_search_hotels", "search_hotels");
function search_hotels(){

    $args = array();

	$hotel_popular_facilities = $_POST["hotel_popular_facilities"];
	$hotel_rating_standard = $_POST["hotel_rating_standard"];
	$min_price = $_POST["min_price"];
	$max_price = $_POST["max_price"];
    

    if(!empty($hotel_popular_facilities)){ 
		    $tax_query =     
		        array("relation"=>"AND",
		        array(
		        'taxonomy' => 'hotel_popular_facilities',
		        'field'    => 'term_id',
		        'terms'    => $hotel_popular_facilities,
		        'operator' => 'IN'
		    ));
		    $args["tax_query"] = $tax_query;    
    }    
    
    if(!empty($hotel_rating_standard)){ 
            $meta_query = array("relation"=>"AND",array(
                'key' => 'hotel_rating_standard',
                'value' => $hotel_rating_standard,
                'compare' => 'IN'
            ));
            $args["meta_query"] = $meta_query;   
    }

    if(!empty($min_price)){
    	$price_range = array($min_price,$max_price);
        $meta_query = array("relation"=>"AND",array(
            'key' => 'hotel_price',
            'value' => $price_range,
            'compare' => 'between',
            'type'    => 'numeric',
        ));
        $args["meta_query"] = $meta_query;     	
    }

    $args["post_type"] = "vr_hotels";
    $args["posts_per_page"] = "-1";
    $args["order"] = "DESC";
    $args["post_status"] = "publish";
    $html="";

   // echo '<pre/>'; 
   // print_r($args);die;

    $loop = new wp_query( $args );
    //echo "Last SQL-Query: {$loop->request}";die;
    ob_start();
    if ( $loop->have_posts() ) {
    	$i=1;
    	$html.="<div class='row'>";
		while ( $loop->have_posts() ) : $loop->the_post();
			$post_id = get_the_ID();
			$title =   get_the_title();
			$hotel_price = get_post_meta($post_id, 'hotel_price', true);
			$location_on_map=get_post_meta($post_id, 'location_on_map', true);
            $latitude=  $location_on_map["lat"];
            $longitude= $location_on_map["lng"];
			$hotel_address =get_post_meta($post_id, 'hotel_address', true);
            $hotel_image = get_the_post_thumbnail($post_id,array(100,100));
            
            $html.="<div class='col-md-4'>";
                $html.=$hotel_image; 
				$html.="<h4><a href='".get_permalink($post_id)."' target='_blank'>".$title."</a></h4>";
				$html.="<p>Address:".$hotel_address."</p>";
				$html .="<hr/>";
			$html.="</div>";
			$map_html=$html;
            $rows[]=array($map_html,$latitude,$longitude,'1');
			$i++;
		endwhile;
		$html.="</div>";
    }
    $output = ob_get_clean();
    if(!empty($html)){
      $response = array("status_code"=>"1","message"=>"Success","result"=>$html,'map_json'=>@$rows);
    }else{
      $response = array("status_code"=>"0","message"=>"No Hotels Found","result"=>array(),'map_json'=>array());
    }
    echo json_encode($response,true);
    die;	
}

