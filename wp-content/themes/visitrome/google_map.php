<style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
    <div id="map"></div>
<?php
  $key = ot_get_option("visit_domain_google_api_key");
  if(empty($key)){
    $key = "AIzaSyBNBkTy4fR0wc7UyYJrOPasx_5J8EPZ5M4";
  }
?>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=<?php echo $key;?>&callback=initMap">
</script>
<script>
// Initialize and add the map
    function initMap() {
      var latlng = {lat: <?php echo $latitude;?>, lng: <?php echo $longitude;?>};
      var map = new google.maps.Map(
          document.getElementById('map'), {zoom: 4, center: latlng});
      var marker = new google.maps.Marker({position: latlng, map: map});
    }
</script>

