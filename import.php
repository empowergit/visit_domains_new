<?php
require_once("wp-load.php");
  $list= get_users();
  if(!empty($list)){
   $user_id= @$list[0]->data->ID;
   if(!empty($user_id)){
      $user_id = "1";
   }
  }else{
   $user_id = "1";
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Import Data</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <style type="text/css">
  	.badge-success {
	    height: 25px !important;
	    width: auto !important;
	    font-size: 16px !important;
    }
  </style>
</head>
<body>
<div class="jumbotron text-center">
  <h1>Import data from booking.com</h1>
  <div class="row">
     <div class="col-md-2" style="margin: 0 auto;">
        <div class="form-group">
               <label>Select Your Location</label>
               <select name="location" id="location" class="form-control">
                  <?php
                   $loop = new WP_Query( array( 'post_type' => 'vr_locations', 'posts_per_page' => -1 ) );
                   while ( $loop->have_posts() ) : $loop->the_post();
                   $post_title = get_the_title();
                   $ID = get_the_ID();
                  ?>
                  <option value="<?php echo $ID;?>"><?php echo $post_title;?></option>
                  <?php
                  endwhile;
                  ?>      
               </select>   
           </div>     
     </div>
  </div>
  <div class="message" style="display: none;margin-bottom: 10px;">Progress.....</div>
  <button type="button" onclick="import_data();" class="btn btn-primary">Import data</button>
</div>
</body>
</html>
<script type="text/javascript">
	function import_data(){
		    jQuery(".message").show().html("<div><img src='https://www.jotform.com/images/ajax-loader.gif'/></div>");
        var location = jQuery("#location option:selected").val();
            jQuery.ajax({
            type: "POST",
            data: {user_id:'<?php echo $user_id;?>',location:location},
            url: "import_data.php",
            success: function (result) {
                var http_response_code = '<?php echo http_response_code();?>';
                if(http_response_code == "200"){
                    console.log("success");
                    jQuery(".message").show().html("<div class='badge badge-success'>Imported Successfully</div>");
                }else{
                    jQuery(".message").show().html("<div><img src='https://www.jotform.com/images/ajax-loader.gif'/></div>");
                }
            }
        });	
	}
</script>

