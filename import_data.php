<?php

if(!empty($_POST)){
  $user_id = !empty($_POST["user_id"])?$_POST["user_id"]:'1';
  $location = !empty($_POST["location"])?$_POST["location"]:'35';
}

require_once("wp-load.php");
// From URL to get webpage contents. 
//$url = "https://api.apify.com/v2/datasets/sFhF66gXQjhWhhc4R/items"; 


// https://api.apify.com/v2/actor-tasks/RjeXrNfZuoczo8qqt/runs/last/dataset/items?token=gXqD25M9AeqMDxfA6vLHS3ZwQ

$url= "https://api.apify.com/v2/actor-tasks/BeFZiv66vsAEzzhxx/runs/last/dataset/items?token=yw7gopCjpZbSZBhysPrdb3EqR";
  
// Initialize a CURL session. 
$ch = curl_init();  
  
// Return Page contents. 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
  
//grab URL and pass it to the variable. 
curl_setopt($ch, CURLOPT_URL, $url); 
  
$result = curl_exec($ch); 
  
if(!empty($result)){
  $json= json_decode($result,true);
  /*echo '<pre/>';
  print_r($json);*/
  for($i=0;$i<4;$i++){
  //for($i=0;$i<count($json);$i++){
    $map_zoom="8";
    $price = trim($json[$i]["price"]);
    $price_avg ="0";
    if(!empty($price)){
      preg_match_all('!\d+!', $price, $matches);
      if(!empty($matches)){
        $price_avg = @$matches[0][0];
      }
    }
    if($price_avg == 0){
      continue;
    } 
    $hotel_star= trim($json[$i]["rating"]);
  	$title = $json[$i]["title"];
    $post_slug = sanitize_title_with_dashes ($title,'','save');
    $post_slugsan = sanitize_title($post_slug); 
  	$data["post_title"] = $json[$i]["title"];
  	$data["post_content"] = $json[$i]["description"];
  	$data["post_date"] = date("Y-m-d H:i:s");
  	$data["post_date_gmt"] = date("Y-m-d H:i:s");
  	$data["post_status"] = "publish";
  	$data["post_type"] = "vr_hotels";
  	$data["post_author"]=$user_id;
  	$data["post_name"]=$post_slugsan;
  	global $wpdb;
  	$wpdb->insert("wp_posts",$data);
    $last_id = $wpdb->insert_id;

    $latlong=@explode(",",$json[$i]["latlong"]);

    $hotel_location=$location;
    update_post_meta($last_id,"hotel_location",$hotel_location);
    update_post_meta($last_id,"hotel_address",$json[$i]["address"]);
    update_post_meta($last_id,"hotel_rating_standard",$hotel_star);
    update_post_meta($last_id,"hotel_price",$price_avg);
    update_post_meta($last_id,"hotel_policy",$json[$i]["hotel_policy"]);
    update_post_meta($last_id,"booking_com_url",$json[$i]["url"]);

  	$st_google_map["lat"] = @$latlong[0];
  	$st_google_map["lng"] = @$latlong[1];
  	$st_google_map["address"] = $json[$i]["address"];
  	$map = serialize($st_google_map);
  	$wpdb->insert("wp_postmeta",array("meta_key"=>'location_on_map',"meta_value"=>$map,"post_id"=>$last_id));

     
     
     
     
    $hotel_image = $json[$i]["hotel_image"];
  	$gallery = array();
    if(!empty($hotel_image)){
	    for($j=0;$j< count($hotel_image);$j++){
          $image_url = $hotel_image[$j]["src"];
          if($image_url != "#"){
              $exp=explode("/",$image_url);
              $image_url="https://q-cf.bstatic.com/images/hotel/max1024x768/".$exp[6]."/".$exp[7];
        		 $wordpress_upload_dir = wp_upload_dir();
        		 $url = $image_url;
        		 $name = pathinfo($url,PATHINFO_EXTENSION);
        		 $new_file_mime ="image/".$name;
        		 $profilepicture['name'] = basename($url);
        		 $new_file_path = $wordpress_upload_dir['path'] . '/' . $profilepicture['name'];
        		 $status= @file_put_contents($new_file_path, file_get_contents($url));
        		  if(!empty($status)){
        			  $upload_id = wp_insert_attachment( array(
        			    'guid'           => $new_file_path, 
        			    'post_mime_type' => $new_file_mime,
        			    'post_title'     => preg_replace( '/\.[^.]+$/', '', $profilepicture['name'] ),
        			    'post_content'   => '',
        			    'post_status'    => 'inherit'
        			  ), $new_file_path );

                require_once(ABSPATH . 'wp-admin/includes/image.php');
                $attach_data = wp_generate_attachment_metadata( $upload_id, $new_file_path );
                wp_update_attachment_metadata( $upload_id, $attach_data );
    	           array_push($gallery,$upload_id);
    	           if($j == 0){
              	     setFeaturedImage($last_id,$image_url);
                   }
               }
          }
	    }
    }
    $gallery = serialize($gallery);
    $wpdb->insert("wp_postmeta",array("meta_key"=>'hotel_gallery',"meta_value"=>$gallery,"post_id"=>$last_id));

   $popular_facilities = $json[$i]["popular_facilities"];
   foreach($popular_facilities as $key=>$val){
      $popular_facilities = $val;
      $start_point = strpos($popular_facilities, '<svg');
      $end_point = strpos($popular_facilities, '</svg>', $start_point);
      $length = $end_point-$start_point;
      $icon = substr($popular_facilities, $start_point, $length);
      $popular_facilities = strip_tags($popular_facilities);
      setHotelPopularFacilities($last_id,trim($popular_facilities),$icon);
   }

   $common_facilities = $json[$i]["common_facilities"];
   foreach($common_facilities as $key=>$val){
      $common_facilities_parent = $val["parent"];
      $start_point = strpos($common_facilities_parent, '<svg');
      $end_point = strpos($common_facilities_parent, '</svg>', $start_point);
      $length = $end_point-$start_point;
      $icon = substr($common_facilities_parent, $start_point, $length);
      $common_facilities_child = "<ul>".$val["child"]."</ul>";
      $common_facilities_parent = strip_tags($common_facilities_parent);
      setHotelFacilities($last_id,trim($common_facilities_parent),$common_facilities_child,$icon);
   }

  }	
}


function setHotelPopularFacilities($post_id,$facilities,$icon){
    $taxonomy = "hotel_popular_facilities";
         $cat  = get_term_by('name', $facilities , $taxonomy);
         if($cat == false){
           $cat = wp_insert_term($facilities,$taxonomy);
           $cat_id = $cat['term_id'] ;
         }else{
           $cat_id = $cat->term_id ;
         }
         update_term_meta($cat_id, "hotel_popular_facility_icon", $icon);
         $res=wp_set_post_terms($post_id,array($cat_id),$taxonomy ,true);
}



function setHotelFacilities($post_id,$facilities,$items,$icon){
    $taxonomy = "hotel_facilities";
         $cat  = get_term_by('name', $facilities , $taxonomy);
         if($cat == false){
           $cat = wp_insert_term($facilities,$taxonomy);
           $cat_id = $cat['term_id'] ;
         }else{
           $cat_id = $cat->term_id ;
         }
         update_term_meta($cat_id, "hotel_facility_icon", $icon);
         update_term_meta($cat_id, "hotel_facility_items", $items);
         $res=wp_set_post_terms($post_id,array($cat_id),$taxonomy ,true);
} 

function setFeaturedImage($post_id,$image_url){
	$wordpress_upload_dir = wp_upload_dir();
	$url = $image_url;
	$name = pathinfo($url,PATHINFO_EXTENSION);
	$new_file_mime ="image/".$name;
	$picture['name'] = basename($url);
	$new_file_path = $wordpress_upload_dir['path'] . '/' . $picture['name'];
	$status= @file_put_contents($new_file_path, file_get_contents($url));
	if(!empty($status)){
	    $attachment = array(
	        'post_mime_type' => $new_file_mime,
	        'post_title' => sanitize_file_name($picture['name']),
	        'post_content' => '',
	        'post_status' => 'inherit'
	    );
	    $attach_id = wp_insert_attachment( $attachment, $new_file_path, $post_id );
	    require_once(ABSPATH . 'wp-admin/includes/image.php');
	    $attach_data = wp_generate_attachment_metadata( $attach_id, $new_file_path );
	    wp_update_attachment_metadata( $attach_id, $attach_data );
	    set_post_thumbnail( $post_id, $attach_id );		 	
	}    
} 
?>