#Theme Customizer

https://www.cssigniter.com/how-to-create-a-custom-color-scheme-for-your-wordpress-theme-using-the-customizer/
https://getflywheel.com/layout/how-to-add-options-to-the-wordpress-customizer/

Create a new Section in customizer:

<?php
add_action( 'customize_register', 'theme_customize_register' );
function theme_customize_register(){
	$wp_customize->add_section('your_theme_footer', array(
	'title' => 'Footer',
	'description' => '',
	'priority' => 120,
	));
	$wp_customize->add_setting( 'custom_field' , array(
	    'default' => 'Default value',
	    'type' => 'theme_mod',
	    'sanitize_callback' => 'our_sanitize_function',
	) );

	$wp_customize->add_control('custom_field', array(
	    'label' => __( 'Custom text for custom field', 'placeholderfortextdomain' ),
	    'section' => 'your_theme_footer',
	    'settings' => 'custom_field',
	    'type' => 'text'
	));	
}
function our_sanitize_function( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}
?>

#Theme Option:
Usage: <?php ot_get_option( $option_id, $default ); ?>

1. Copyright :   visit_domain_copyright
2. Social Links: visit_domain_social_links


#Custom Field:
1. Hotel Location: hotel_location
2. Hotel address: hotel_address
3. Location on map: location_on_map


#Google Map:
API Key: AIzaSyBNBkTy4fR0wc7UyYJrOPasx_5J8EPZ5M4

AIzaSyAqzY0Hhsc6E71q1amF5XaBGaE1EY3nA4E

https://www.advancedcustomfields.com/resources/google-map/









